package com.example.layoutdanevent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLogin;
    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(!username.getText().toString().equals("user") && !password.getText().toString().equals("1234")){
            Toast.makeText(this, "Username dan Password salah", Toast.LENGTH_SHORT).show();
        } else if (!username.getText().toString().equals("user")) {
            Toast.makeText(this, "Username salah", Toast.LENGTH_SHORT).show();
        } else if (!password.getText().toString().equals("1234")) {
            Toast.makeText(this, "Password salah", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Username dan Password benar", Toast.LENGTH_SHORT).show();
        }
    }
}